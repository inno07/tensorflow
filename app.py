import json
import os

import numpy
from flask import Flask, Response, jsonify
import requests
from werkzeug.utils import secure_filename
from flask import Flask, flash, request

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import tensorflow as tf

from tensorflow import keras
# from tensorflow.keras import layers
from keras import layers
import classification

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


# x: given value to predict y
# y: predicted value from x
@app.route('/tensorflow', methods=['POST'])
def createModel():
    # get file from request
    file = request.files['file']
    fname = file.filename + '.png'

    # load content of file
    raw_dataset = pd.read_csv(file.stream)
    dataset = raw_dataset.copy()

    firstLine = dataset.iloc[1]
    firstCol = dataset.iloc[:,0]
    toDrop = firstLine.str.contains(pat='[A-Za-z]', regex=True)

    idCol = all(i < j for i, j in zip(firstCol, firstCol[1:]))

    if idCol == True:
        dataset.drop(dataset.columns[[0]], inplace=True, axis=1)

    for name,drop in toDrop.items():
        if drop == True:
            dataset.drop(name, inplace=True, axis=1)

    dataset.tail()

    # split up in train and test sets
    train_dataset = dataset.sample(frac=0.8, random_state=0)
    test_dataset = dataset.drop(train_dataset.index)

    x = request.args.get("x")
    y = request.args.get("y")
    label = y
    key = x

    # split up in features and labels
    train_features = train_dataset.copy()
    test_features = test_dataset.copy()

    train_labels = train_features.pop(label)
    test_labels = test_features.pop(label)

    # create normalizer
    normalizer = tf.keras.layers.Normalization(axis=-1)

    # normalize train features
    normalizer.adapt(np.array(train_features))

    # extract values
    rotspeed = np.array(train_features[key])

    # create normalizer for given columns
    rotspeed_normalizer = layers.Normalization(input_shape=[1, ], axis=None)
    rotspeed_normalizer.adapt(rotspeed)

    # method to generate a plot containing the regression
    def plot_rotspeed(x, y):
        plt.scatter(train_features[key], train_labels, label='Data')
        plt.plot(x, y, color='k', label='Predictions')
        plt.xlabel(key)
        plt.ylabel(label)
        plt.legend()
        plt.savefig(fname)
        # plt.show()

    # create model
    def build_and_compile_model(norm):
        model = keras.Sequential([
            norm,
            layers.Dense(64, activation='relu'),
            layers.Dense(64, activation='relu'),
            layers.Dense(1)
        ])

        model.compile(loss='mean_absolute_error', optimizer=tf.keras.optimizers.Adam(0.001))
        return model

    dnn_rotspeed_model = build_and_compile_model(rotspeed_normalizer)

    # train the model
    history = dnn_rotspeed_model.fit(
        train_features[key],
        train_labels,
        validation_split=0.2,
        verbose=0,
        epochs=10)

    # range of predictions
    x = tf.linspace(1000, 3500, 3501)
    # predictions
    y = dnn_rotspeed_model.predict(x)

    jsondata = {}
    lineData = []
    scatterData = []

    x = x.numpy().tolist()
    y = numpy.reshape(y, -1).tolist()
    for i in range(0, len(x)):
        lineData += [{"name": x[i], "value": y[i]}]

    x = train_features[key].values.tolist()
    y = train_labels.values.tolist()
    for i in range(0, len(x)):
        scatterData += [{"name": i, "x": x[i], "y": y[i], "r": 59.4}]

    jsondata['line'] = {'name': file.filename, 'series': lineData}
    jsondata['scatter'] = {'name': file.filename, 'series': scatterData}
    jsondata['info'] = {'loss': history.history['loss'][9]}

    response = Response(json.dumps(jsondata), mimetype='application/json', status=201)

    return response

@app.route('/classification', methods=['POST'])
def classificationAlgo():
    trainDataFile = request.files['trainFile']
    predictDataFile = request.files['dataFile']
    dataset = pd.read_csv(predictDataFile.stream)
    dependentCols = request.args.get("dependentCols")
    predict_colname = request.args.get("parameterCol")
    result = classification.classificationWithParameter(trainDataFile, dependentCols, predict_colname, dataset.copy())

    data = {"head": list(dataset.columns.values), "toPredict": predict_colname, "data": []}
    index = 0
    for percentage in result.tolist():
        data["data"] += [{"colValues": dataset.iloc[index].tolist(), "percentage": percentage[0]}]
        index = index + 1

    print(data)
    response = Response(json.dumps(data), mimetype='application/json', status=201)
    return response

@app.route('/classificationNoArg', methods=['POST'])
def classificationAlgoNoArg():
    trainDataFile = request.files['trainFile']
    predictDataFile = request.files['dataFile']
    dataset = pd.read_csv(predictDataFile.stream)
    dependentCols = request.args.get("dependentCols")
    predict_colname = request.args.get("parameterCol")
    limit = request.args.get("limit")
    result = classification.classificationWithNoParameter(trainDataFile, dependentCols, predict_colname, limit, dataset.copy())

    data = {"head": list(dataset.columns.values), "toPredict": predict_colname, "data": []}
    index = 0
    for percentage in result.tolist():
        data["data"] += [{"colValues": dataset.iloc[index].tolist(), "percentage": percentage[0]}]
        index = index + 1

    response = Response(json.dumps(data), mimetype='application/json', status=201)
    return response


if __name__ == '__main__':
    app.run(port=5050, debug=True, host='0.0.0.0')
