FROM python:3.10-bullseye

WORKDIR /usr/src/app

COPY . .

RUN python -m pip install --upgrade pip
RUN pip uninstall protobuf
RUN pip install --no-binary protobuf protobuf
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5050

CMD [ "python", "app.py" ]
