import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import tensorflow as tf


def get_map(df, name):
    col = df[name]
    m = {}
    idx = 0
    for i in col:
        if i not in m:
            m[i] = idx
            idx += 1
    return m


def str_to_number(df, map_):
    for mi in map_:
        df.replace(mi, map_[mi], inplace=True)

def classificationWithParameter(trainDataFile, dependentCols, predict_colname, predictData):
    trainData = pd.read_csv(trainDataFile.stream)

    for name, column in trainData.items():
        dtype = column.dtype
        if dtype == object:
            m = get_map(trainData, name)
            str_to_number(trainData, m)
            str_to_number(predictData, m)

    for colname in trainData.columns.values:
        if colname not in dependentCols and colname != predict_colname:
            trainData.drop(colname, inplace=True, axis=1)

    return classificationCore(trainData, predict_colname, predictData)


def classificationWithNoParameter(trainDataFile, dependentCols, predict_colname, limit, predictData):
    trainData = pd.read_csv(trainDataFile.stream)
    limit = float(limit)

    for name, column in trainData.items():
        dtype = column.dtype
        if dtype == object:
            m = get_map(trainData, name)
            str_to_number(trainData, m)
            str_to_number(predictData, m)

    trainData["to_predict"] = [1 if v >= limit else 0 for v in trainData[predict_colname]]
    trainData.drop(predict_colname, axis=1, inplace=True)

    for colname in trainData.columns.values:
        if colname not in dependentCols and colname != predict_colname and colname != "to_predict":
            trainData.drop(colname, inplace=True, axis=1)

    return classificationCore(trainData, "to_predict", predictData)


def classificationCore(trainData, predict_colname, predictData):
    X = trainData.drop(predict_colname, axis=1)
    Y = trainData[predict_colname]

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)

    tf.random.set_seed(42)

    model = tf.keras.Sequential([
        tf.keras.layers.Dense(128, activation="relu"),
        tf.keras.layers.Dense(256, activation="relu"),
        tf.keras.layers.Dense(256, activation="relu"),
        # tf.keras.layers.Dense(128, activation="relu"),
        tf.keras.layers.Dense(1, activation="sigmoid")
    ])

    model.compile(
        loss=tf.keras.losses.binary_crossentropy,
        optimizer=tf.keras.optimizers.Adam(learning_rate=0.03),
        metrics=[
            tf.keras.metrics.BinaryAccuracy(name="accuracy"),
            tf.keras.metrics.Precision(name="precision"),
            tf.keras.metrics.Recall(name="recall")
        ]
    )

    history = model.fit(X_train_scaled, Y_train, epochs=30)

    prediction = model.predict(scaler.transform(predictData))

    percentage = 100 * prediction
    return percentage